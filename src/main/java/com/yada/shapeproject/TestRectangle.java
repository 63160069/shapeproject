/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.shapeproject;

/**
 *
 * @author ASUS
 */
public class TestRectangle {
    public static void main(String[] args) {
      Rectangle rectangle1 = new Rectangle(2,3);
      System.out.println("Area of rectangle1 (h = "+ rectangle1.getH()+",w = "+rectangle1.getW()+") is " + rectangle1.calArea());  
      rectangle1.setH(3);//rectangle1.h = 3;
      rectangle1.setW(4);//rectangle1.w = 4;
      System.out.println("Area of rectangle1 (h = "+ rectangle1.getH()+",w = "+rectangle1.getW()+") is " + rectangle1.calArea());  
      rectangle1.setH(0);//rectangle1.h = 0;
      System.out.println("Area of rectangle1 (h = "+ rectangle1.getH()+",w = "+rectangle1.getW()+") is " + rectangle1.calArea());  
      rectangle1.setW(0);//rectangle1.w = 0;
      System.out.println("Area of rectangle1 (h = "+ rectangle1.getH()+",w = "+rectangle1.getW()+") is " + rectangle1.calArea());  
      System.out.println(rectangle1.toString()); 
      System.out.println(rectangle1);
    }
    
}
